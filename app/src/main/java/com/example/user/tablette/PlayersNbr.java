package com.example.user.tablette;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PlayersNbr extends AppCompatActivity {

    public Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players_nbr);

        context = this;
        Button submit = findViewById(R.id.validButton);
        submit.setEnabled(false);
        EditText nbrPlayers = findViewById(R.id.nbrPlayersValue);

        nbrPlayers.addTextChangedListener(new TextWatcher() {
            EditText nbrPlayers = findViewById(R.id.nbrPlayersValue);
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (nbrPlayers.getText().toString().length() <= 0){
                    nbrPlayers.setError("Required");
                    findViewById(R.id.validButton).setEnabled(false);
                }
                else {
                    if (Integer.parseInt(nbrPlayers.getText().toString()) < 2) {
                        nbrPlayers.setError("Minimum 2");
                        findViewById(R.id.validButton).setEnabled(false);
                    }
                    else if(Integer.parseInt(nbrPlayers.getText().toString()) > 5){
                        nbrPlayers.setError("Maximum 5");
                        findViewById(R.id.validButton).setEnabled(false);
                    }
                    else {
                        findViewById(R.id.validButton).setEnabled(true);
                    }
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText nbrPlayers = findViewById(R.id.nbrPlayersValue);
                Intent i = new Intent(context, PlayersFiche.class);
                i.putExtra("nbrPlayers", Integer.parseInt(nbrPlayers.getText().toString()));
                i.putExtra("indice", 0);
                startActivity(i);
            }
        });

        Button cancel = findViewById(R.id.cancelButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, index.class);
                startActivity(i);
            }
        });
    }
}
