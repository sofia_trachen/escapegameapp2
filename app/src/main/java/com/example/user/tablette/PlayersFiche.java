package com.example.user.tablette;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;


public class PlayersFiche extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_GALERY = 2;
    private int remainingPlayers,indice;
    public Context context;
    char tableauCaractere[] = {'A','B','C','D','E','F','G'};
    char ficheLetter;
    private Uri pictureUri;
    private String pathToPicture;
    private String imgDecodableString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players_fiche);

        TextView ficheX = findViewById(R.id.ficheText);
        ficheX.append(""+tableauCaractere[getIntent().getIntExtra("indice", 0)]);
        ficheLetter = tableauCaractere[getIntent().getIntExtra("indice", 0)];
        context = this;
        remainingPlayers = getIntent().getIntExtra("nbrPlayers", -1) - 1;
        indice = getIntent().getIntExtra("indice", 0) + 1;
        Button valid = findViewById(R.id.validButton);

        EditText prenomTextFiltre = findViewById(R.id.prenomText);
        prenomTextFiltre.setFilters(new InputFilter[] {
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start, int end, Spanned spanned, int dStart, int dEnd) {
                        if(cs.equals("")){ // for backspace
                            return cs;
                        }
                        if(cs.toString().matches("[a-zA-Z ]+")){
                            return cs;
                        }
                        return "";
                    }
                }
        });

        valid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getIntent().hasExtra("_id")) {
                    //Edit players
                    String _id = getIntent().getStringExtra("_id");
                    Log.d("Joueur a modifier id ", _id + "");
                    new EditData().execute(_id, pathToPicture);

                }
                else {
                    EditText prenomText = findViewById(R.id.prenomText);
                    Intent i = new Intent(context, PlayersFiche.class);
                    Intent iList = new Intent(context, PlayersList.class);
                    i.putExtra("nbrPlayers", remainingPlayers);
                    i.putExtra("indice", indice);
                    if(remainingPlayers > 0) {
                        //new PostData().execute(prenomText.getText().toString(),""+ficheLetter, pathToPicture);
                        startActivity(i);
                    }
                    else if (remainingPlayers == 0){
                        //new PostData().execute(prenomText.getText().toString(),""+ficheLetter, pathToPicture);
                        startActivity(iList);
                    }
                }
            }
        });

        final Button cancel = findViewById(R.id.cancelButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, index.class);
                startActivity(i);
            }
        });

        // -------------------------- Picture Treatment -------------------------

        Button b = (Button)findViewById(R.id.takePicture);

        b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, pictureUri);
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
        });

        findViewById(R.id.takePictureBis).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent galeryIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galeryIntent, REQUEST_IMAGE_GALERY);
            }
        });
        // -----------------------------------------------------------------------

        if(getIntent().hasExtra("name")) {
            EditText prenomText = findViewById(R.id.prenomText);
            prenomText.setHint(getIntent().getExtras().getString("name"));
            prenomText.setEnabled(false);
        }

        if(getIntent().hasExtra("imageId")) {
            ImageView imageView = findViewById(R.id.pictureView);
            new DownloadImageTask(imageView).execute(getIntent().getExtras().getString("imageId"));
            findViewById(R.id.takePicture).setVisibility(View.GONE);
            findViewById(R.id.photoBis).setVisibility(View.VISIBLE);
            findViewById(R.id.pictureViewBis).setVisibility(View.VISIBLE);
            findViewById(R.id.takePictureBis).setVisibility(View.VISIBLE);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            Bitmap image = (Bitmap) data.getExtras().get("data");
            Uri tempUri = getImageUri(getApplicationContext(), image);
            String uri = getRealPathFromURI((tempUri));
            pathToPicture = uri;
            ImageView imageview = (ImageView) findViewById(R.id.pictureView); //sets imageview as the bitmap
            imageview.setImageBitmap(image);
        }

        if (requestCode == REQUEST_IMAGE_GALERY) {
           /* Bitmap image = (Bitmap) data.getExtras().get("data");
            Uri tempUri = getImageUri(getApplicationContext(), image);
            String uri = getRealPathFromURI((tempUri));
            pathToPicture = uri;
            ImageView imageview = (ImageView) findViewById(R.id.pictureViewBis); //sets imageview as the bitmap
            imageview.setImageBitmap(image); */

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            pathToPicture = filePathColumn[0];
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgDecodableString = cursor.getString(columnIndex);
            cursor.close();
            ImageView imgView = (ImageView) findViewById(R.id.pictureViewBis);
            imgView.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public class PostData extends AsyncTask< String, Void, Boolean > {

        AlertDialog.Builder alertDialog;
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = new AlertDialog.Builder(PlayersFiche.this);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            Boolean sentData = true;
            HttpPost httppost = new HttpPost("http://vps302763.ovh.net:7878/player/add/");
            try {
                String Name = params[0];
                String Id = params[1];
                //String playerPhoto = params[2];
                FileBody contentFile = new FileBody(new File(pathToPicture));
                MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                reqEntity.addPart("name",new StringBody(Name));
                reqEntity.addPart("id",new StringBody(Id));
                reqEntity.addPart("playerPhoto",contentFile);
                httppost.setEntity(reqEntity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity resEntity = response.getEntity();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
               return false;
            }catch (Exception e){
                e.printStackTrace();
            }
            return sentData;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            try {
                super.onPostExecute(result);
                if (!result){
                    alertDialog.setTitle("Problème Connexion");
                    alertDialog.setMessage("Redemarrez le serveur !");
                    alertDialog.setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alertDialog.show();
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public class EditData extends AsyncTask< String, Void, Void > {
        @Override
        protected Void doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://vps302763.ovh.net:7878/player/update/");
            try {
                String _Id = params[0];
                FileBody contentFile = new FileBody(new File(pathToPicture));
                MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                reqEntity.addPart("_id",new StringBody(_Id));
                reqEntity.addPart("playerPhotoBis",contentFile);
                httppost.setEntity(reqEntity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity resEntity = response.getEntity();

            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
    }
}

class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    private ProgressDialog mDialog;
    private ImageView bmImage;

    public DownloadImageTask(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    protected void onPreExecute() {
        //mDialog = ProgressDialog.show(ChartActivity.this,"Please wait...", "Retrieving data ...", true);
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", "image download error");
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        //set image of your imageview
        bmImage.setImageBitmap(result);
        //close
        // mDialog.dismiss();
    }
}


