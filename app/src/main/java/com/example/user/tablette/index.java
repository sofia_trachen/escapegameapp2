package com.example.user.tablette;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class index extends AppCompatActivity {
    public Context context;

    String url = "http://google.com";
    public String [] ids;

    JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
            (url, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    if (response != null && response.length() > 0 ){
                        ids = new String[response.length()];
                        if (ids.length == 0){
                            Intent i = new Intent(context, PlayersNbr.class);
                            startActivity(i);
                        }
                        else {
                            TextView nbPlayers = findViewById(R.id.nbPlayers);
                            if (ids.length == 1){
                                nbPlayers.setText(String.valueOf(ids.length)+" Joueur");
                            }
                            else {
                                nbPlayers.setText(String.valueOf(ids.length)+" Joueurs");
                            }
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(index.this);
                    alertDialog.setTitle("Problème Connexion");
                    alertDialog.setMessage("Redemarrez le serveur !");
                    alertDialog.setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }
                    });
                    alertDialog.show();
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //RequestQueue requestQueue = Volley.newRequestQueue(this);
        //requestQueue.add(jsonObjectRequest);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        context = this;
        Button reset = findViewById(R.id.resetButton);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setCancelable(true);
                builder.setTitle("Confirmation");
                builder.setMessage("Etes-vous sûre ?");
                builder.setPositiveButton("OUI",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new RemoveData().execute();
                                Intent i = new Intent(context, PlayersNbr.class);
                                startActivity(i);
                            }
                        });
                builder.setNegativeButton("NON", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        Button Visualiser = findViewById(R.id.visualizeButton);
        Visualiser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, PlayersList.class);
                startActivity(i);
            }
        });
    }

    public class RemoveData extends AsyncTask< Void, Void, Void > {
        @Override
        protected Void doInBackground(Void... voids) {
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL("http://vps302763.ovh.net:7878/player/removeAll");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null){
                    urlConnection.disconnect();
                }
            }
            return null;
        }
    }

}