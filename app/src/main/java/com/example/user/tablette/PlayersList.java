package com.example.user.tablette;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.ArrayList;
import android.util.Log;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

public class PlayersList extends Activity {

    ListView lv;
    Context context;
    PlayersList ma = this;
    String url = "http://137.74.175.78:7878/player";
    public String [] prgmNameList;
    public String [] prgmImages;
    public String [] prgm_ids;

    JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
            (url, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    //mTextView.setText("Response: " + response.toString());
                    prgmNameList = new String[response.length()];
                    prgmImages = new String[response.length()];
                    prgm_ids = new String[response.length()];

                    for(int i = 0; i < response.length(); i++) {
                        try {
                            prgm_ids[i] = response.getJSONObject(i).getString("_id");
                            prgmNameList[i] = response.getJSONObject(i).getString("name");
                            //prgmImages[i] = "http://137.74.175.78:7878/player/images/" + response.getJSONObject(i).getString("photoFileName");

                        } catch(JSONException e) {
                            Log.e("probleme", "probleme");
                            e.printStackTrace();
                        }
                    }
                    lv=(ListView) findViewById(R.id.listView);

                    lv.setAdapter(new Adapter(ma, prgmNameList,prgmImages,prgm_ids));
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO: Handle error
                    Log.e("probleme", "probleme");
                }
            });
    ArrayList prgmName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(jsonObjectRequest);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_main);
        context=this;

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(context, index.class);
                startActivity(i);
            }
        });


        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
